require 'spec_helper'

describe 'deconstruct these tidbits' do
  it '!?! must be false' do
    answer = !?!
    answer.must_equal false
  end
  it '!!?! must be true' do
    answer = !!?!
    answer.must_equal true
  end
  it '?:??::?? must be ":"' do
    answer = ?:??::??
    answer.must_equal ':' 
  end
end

